[![pipeline status](https://gitlab.com/frenchtoasters/todobot/badges/master/pipeline.svg)](https://gitlab.com/frenchtoasters/todobot/commits/master)
[![coverage_status](https://gitlab.com/frenchtoasters/todobot/badges/master/coverage.svg)](https://gitlab.com/frenchtoasters/todobot/badges/master/coverage.svg)

# Todobot

`Todobot` is a set of automation tasks that will preform some of the more mundane Gitlab operations based on a `python` 
comment structure.  

## Import 

To import this project you simply have to add the following to your projects `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://gitlab.com/frenchtoasters/todobot/raw/master/todo_yaml/todobot-issues.yml'
  - remote: 'https://gitlab.com/frenchtoasters/todobot/raw/master/todo_yaml/todobot-milestones.yml'
  - remote: 'https://gitlab.com/frenchtoasters/todobot/raw/master/todo_yaml/todobot-wiki.yml'
```

## Tasks

The following is the list of currently supported tasks:

* Issues, [example](https://gitlab.com/frenchtoasters/todobot/-/blob/master/tests/issues_test.py)
    * Syntax
        ```python
        # TODO::Test todo line new
              # NOTES::single test notes line
          # TAGS::mile=singlelineepic1,app=single
        
        # TODO::multi-line todo test new
        # NOTES::the first part
        # TAGS::mile=multilineepic1,app=multi
        ```
* Milestones, [example](https://gitlab.com/frenchtoasters/todobot/-/blob/master/tests/issues_test.py)
    * Syntax:
        - `# TAGS::mile=NAMEOFMILESTONE,app=MILESTONE,place=DESCRIPTION`

* Wiki, [example](https://gitlab.com/frenchtoasters/todobot/-/blob/master/tests/issues_test.py)
    * Syntax:
        ```python
        # :: /# Header line
        # :: Nomral
        # :: _italics_
        # :: *bold*
        # :: //always have to end with this blank
        # ::
        ```
    * Updating tests to validate all markdown compability

## Setup

The `todobot` uses the `Gitlab` api to interact with your project. In order for that to work a `$todo_bot_token` needs 
to be set in the projects `Settings -> CI/CD -> Variables`. This `todo_bot_token` can be one of the following types:

- Stable
    * [Personal Access Token](https://gitlab.com/profile/personal_access_tokens)

- Iffy
    * [OAuth Token](https://docs.gitlab.com/ee/api/oauth2.html)
    * [Job Token](https://docs.gitlab.com/ee/user/permissions.html#job-permissions) ***Note limited operations***

## Notes

* Excluded files/folders: `"venv", ".git", ".idea", ".md", ".json", "__pycache__", "dist", "todobot.egg-info"`
