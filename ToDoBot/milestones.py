import argparse
from ToDoBot import Todobot


def build_labels(desc: str = None) -> list:
    matched = []
    tags = desc.split(',')
    for tag in tags:
        matched.append(tag)
    matched.append("creator=todobot")
    return matched


parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start("miles")

print("Looking for milestones")
milestones = todo.get_milestones()
for noted in milestones:
    open_milestones = todo.open_milestones
    if noted['title'] not in open_milestones:
        mile = todo.project.milestones.create({
            'id': args.project,
            'title': noted['title'],
            'description': str(build_labels(noted['description']))
        })
        mile.state_event = 'activate'
        mile.save()
        print("Creating Milestone: {}".format(mile.title))
        todo.milestones.extend([mile])
        print("Updating Todo Cache: {}".format(todo.cache.cache['uri']))

# TODO::Unable to link milestones on creation
# NOTES::even though the milestones are added to todo.milestones
# TAGS::mile=multilinepic,app=base,owner=toast

print("Linking Issues to Milestones:\n")
for mile in todo.milestones:
    for child in todo.issues:
        if "mile="+mile.title in child.labels:
            for issue in todo.issues:
                if issue.title == child.title and issue.milestone is None:
                    issue.milestone_id = mile.id
                    issue.save()
                    print("Updating Issue: {}".format(issue.title))
                    continue
                if issue.title == child.title and issue.milestone.get('id') == mile.id:
                    print("Issue: {} == Milestone: {}".format(issue.title, mile.title))
