import gitlab
import os
import re


class BotCache(object):
    def __init__(self, uri: str, project: str):
        self.cache = {
            "uri": uri,
            "project": project,
            "issues": [],
            "milestones": []
        }

    def add_issues(self, issues: list):
        """
        Add new issues to the cache
        :param issues:
        :return:
        """
        self.cache['issues'].extend(issues)

    def add_milestones(self, milestones: list):
        """
        Add new milestone
        :param milestones:
        :return:
        """
        self.cache['milestones'].extend(milestones)

    def __getitem__(self, item):
        if item == "issues":
            return self.cache['issues']
        elif item == "milestones":
            return self.cache['milestones']


class ToDoBot(object):
    def __init__(self, uri: str, token_type: str, token: str, project: str, comment_length: int = 0,
                 todo_issues: list = None, cache: BotCache = None):
        self.issues = None
        self.milestones = None
        self.open_milestones = []
        self.open_issues = []
        self.wiki_content = {
            "title": "todobot_wiki",
            "format": "markdown",
            "content": ""
        }
        self.comment_length = comment_length
        if isinstance(cache, BotCache):
            self.cache = cache
        else:
            self.cache = BotCache(uri, project)

        # Load files content
        self.files = []
        self.files = self._load_files(os.getcwd())

        if isinstance(todo_issues, list):
            self.todo_issues = todo_issues
        else:
            self.todo_issues = []
        if token_type == 'private' or token_type == 'personal':
            self.gl = gitlab.Gitlab(uri, private_token=token)
            self.project = self.gl.projects.get(project)
        elif token_type == 'oauth':
            self.gl = gitlab.Gitlab(uri, oauth_token=token)
            self.project = self.gl.projects.get(project)
        elif token_type == 'job':
            self.gl = gitlab.Gitlab(uri, job_token=token)
            self.project = self.gl.projects.get(project)
        else:
            self.gl = gitlab.Gitlab(uri)
            self.project = self.gl.projects.get(project)

    def _load_files(self, dir_path: str) -> list:
        """
        Load the files of the current project directory
        :param dir_path:
        :return: List of files content
        """
        fulllist = []
        for (dirpath, dirnames, filenames) in os.walk(dir_path):
            fulllist.extend([os.path.join(dirpath, file) for file in filenames])

        check = []
        exclude = ["venv", ".git", ".idea", ".md", ".json", "__pycache__", "dist", "todobot.egg-info"]
        for subdir in fulllist:
            if any(exc in subdir for exc in exclude):
                continue
            else:
                check.append(subdir)

        results = []
        for file in check:
            with open(file, 'r') as f:
                data = f.readlines()
                results.extend(data)
        return results

    def _find_issues(self) -> list:
        """
        Strictly defined process to gather notes
        :return: list of found todos that are in format
        """
        matched = []
        for i in range(len(self.files)):
            result = re.search(r'^\s*(#\sTODO::)|^(#\sTODO::)|^\s*(#\s::)|^(#\s::)', self.files[i])
            if result is not None:
                if "# TODO::" in result.group():
                    found = {
                        "title": self.files[i].strip().strip('# '),
                        "description": self.files[i+1].strip().strip('# NOTES::'),
                        "tags": self.files[i+2].strip().strip('# TAGS::').split(',')
                    }
                    matched.append(found)
                elif "# ::" in result.group() and "TODO" or "NOTES" or "TAGS" not in result.group():
                    """
                    This is where we will figure out how to grab randoms and insert them to something
                    """
                    # TODO::Figure out how to do wildcard linesg
                    # NOTES::We need to figure out what we should do with lines that are wildcard lines.
                    # TAGS::mile=multilineepic,app=backend
                    self.wiki_content['content'] += self.files[i].lstrip().lstrip('# ::').rstrip().replace('/', '')
                    self.wiki_content['content'] += '\n'
        return matched

    def _find_milestones(self) -> list:
        """
        Strictly defined process to gather milestones
        :return: list of milestones that are in format
        """
        matched = []
        for issue in self.cache['issues']:
            found = {
                "title": "",
                "description": ""
            }
            if issue['tags'] != "":
                # Add to milestones
                for tag in issue['tags']:
                    split = tag.split('=')
                    if split[0] == "mile":
                        found['title'] = split[1]
                    else:
                        found['description'] += tag
                if found['title'] != "":
                    matched.append(found)
        return matched

    def _set_open_issues(self):
        """
        Set open issues titles list
        :return:
        """
        for issue in self.issues:
            self.open_issues.append(issue.title)

    def _set_open_milestones(self):
        """
        Set open milestones title list
        :return:
        """
        for epic in self.milestones:
            self.open_milestones.append(epic.title)

    def start(self, task: str = None):
        """
        Start the bot
        :param task: the name of the specific task you are going to run, default is issues
        :return:
        """
        if task == 'miles':
            # Default case to capture basic functionality
            self.issues = self.project.issues.list(state='opened')
            self._set_open_issues()
            self.cache.add_issues(self._find_issues())
            # Additional work for milestones
            self.milestones = self.project.milestones.list(state='active')
            self._set_open_milestones()
            self.cache.add_milestones(self._find_milestones())
        elif task == 'wiki':
            # Default case to capture basic functionality
            self.issues = self.project.issues.list(state='opened')
            self._set_open_issues()
            self.cache.add_issues(self._find_issues())
            # Additional work to manage wiki
            self.wikis = self.project.wikis.list()
        else:
            # Default case to capture basic functionality
            self.issues = self.project.issues.list(state='opened')
            self._set_open_issues()
            self.cache.add_issues(self._find_issues())

    def get_issues(self) -> list:
        """
        Get the issues from the cache
        :return:
        """
        return self.cache['issues']

    def get_milestones(self) -> list:
        """
        Get the milestones from the cache
        :return:
        """
        return self.cache['milestones']