import argparse
from ToDoBot import Todobot


parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start("wiki")

wikis = todo.wikis
for wiki in wikis:
    if wiki.title == "todobot_wiki":
        slug = todo.project.wikis.get(wiki.slug)
        if slug.content != todo.wiki_content['content']:
            slug.content = todo.wiki_content['content']
            slug.save()
            print("Updating Wiki: {}".format(wiki.title))
if len(todo.wikis) == 0:
    page = todo.project.wikis.create(todo.wiki_content)
    page.save()
    print("Creating Wiki: {}".format(page.title))