import argparse
from ToDoBot import Todobot

parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start()

print("Looking for issues")
issues = todo.get_issues()
for noted in issues:
    open_issues = todo.open_issues
    if noted['title'] not in open_issues:
        issue = todo.project.issues.create({'title': noted['title'], 'description': noted['description']})
        noted['tags'].extend(["creator=todobot"])
        issue.labels = noted['tags']
        issue.save()
        print("Creating issue: {}".format(issue.title))
        todo.issues.extend([issue])
        print("Updating Todo Cache: {}".format(todo.cache['uri']))

