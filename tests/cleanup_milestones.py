import argparse
from ToDoBot import Todobot

parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start("miles")

for noted in todo.get_milestones():
    for mile in todo.milestones:
        if noted['title'] == mile.title:
            mile.state_event = 'close'
            print("Closing Milestone: {}".format(mile.title))
            mile.save()
            print("Deleting Milestone: {}".format(mile.title))
            mile.delete()