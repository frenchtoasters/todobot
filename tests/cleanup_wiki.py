import argparse
from ToDoBot import Todobot

parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start("wiki")

for wiki in todo.wikis:
    if wiki.title == "todobot_wiki":
        wiki.delete()
        print("Deleting Wiki: {}".format(wiki.title))