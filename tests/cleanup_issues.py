import argparse
from ToDoBot import Todobot

parser = argparse.ArgumentParser()
parser.add_argument("-t", dest="token", default=None, help="Token used for service access")
parser.add_argument("-p", dest="project", default=None, help="Project ID for service")
args = parser.parse_args()

todo = Todobot.ToDoBot('https://gitlab.com', "personal", args.token, args.project)
todo.start("")

for noted in todo.get_issues():
    for issue in todo.issues:
        if noted['title'] == issue.title:
            issue.state_event = 'close'
            issue.save()
            print("Closing issue: {}".format(issue.title))
