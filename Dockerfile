FROM python:3.8-slim

LABEL maintainer="tylerfrench2@gmail.com"

# Setup user
RUN useradd todo
RUN groupadd todobot
RUN usermod -a -G todobot todo

WORKDIR /opt/todobot

# Set file permissions
RUN chgrp todobot /opt/todobot && chgrp -R todobot /opt/todobot
RUN chown todo /opt/todobot && chown -R todo /opt/todobot

# Copy Static files and requirements
COPY requirements.txt setup.cfg setup.py LICENSE.md README.md /opt/todobot/
RUN pip install -r requirements.txt

# Copy latest object changes
ADD ToDoBot /opt/todobot/ToDoBot

# Install Package
RUN python setup.py install

# Set runtime user
USER todo