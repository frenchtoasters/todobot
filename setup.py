from setuptools import setup, find_packages
import ToDoBot

setup(
    name='todobot',
    version=ToDoBot.__version__,
    author=ToDoBot.__author__,
    description='Todo bot for Gitlab',
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/todobot',
    keywords=['tools', 'gitlab', 'todo', 'bot', 'automation'],
    license=ToDoBot.__license__,
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_data=True,
    install_requires=open('requirements.txt').readlines(),
    entry_points={
        'console_scripts': [
            'tdb=ToDoBot.Todobot:ToDoBot'
        ]
    }
)
